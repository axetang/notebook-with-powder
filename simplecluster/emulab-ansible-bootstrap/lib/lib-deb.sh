#!/bin/sh

#
# Setup apt-get to not prompt us
#
if [ ! -e $OURDIR/apt-configured ]; then
    echo "force-confdef" | $SUDO tee -a /etc/dpkg/dpkg.cfg.d/emulab
    echo "force-confold" | $SUDO tee -a /etc/dpkg/dpkg.cfg.d/emulab
    touch $OURDIR/apt-configured
fi
export DEBIAN_FRONTEND=noninteractive
# -o Dpkg::Options::="--force-confold" -o Dpkg::Options::="--force-confdef" 
DPKGOPTS=''
APTGETINSTALLOPTS='-y'
APTGETINSTALL="$SUDO apt-get $DPKGOPTS install $APTGETINSTALLOPTS"
# Don't install/upgrade packages if this is not set
if [ ${DO_APT_INSTALL} -eq 0 ]; then
    APTGETINSTALL="/bin/true ${APTGETINSTALL}"
fi

update_package_cache() {
    if [ ! "${DO_PACKAGE_UPDATE}" = "1" ]; then
	return
    else
	while [ ! -f $OURDIR/package-cache-updated ]; do
	    $SUDO apt-get -y update \
		&& touch $OURDIR/package-cache-updated \
		|| sleep 8
	done
    fi
}

are_packages_installed() {
    retval=1
    while [ ! -z "$1" ] ; do
	dpkg -s "$1" >/dev/null 2>&1
	if [ ! $? -eq 0 ] ; then
	    retval=0
	fi
	shift
    done
    return $retval
}

maybe_install_packages() {
    update_package_cache
    if [ ! ${DO_PACKAGE_UPGRADE} -eq 0 ] ; then
        # Just do an install/upgrade to make sure the package(s) are installed
	# and upgraded; we want to try to upgrade the package.
	$APTGETINSTALL $@
	return $?
    else
	# Ok, check if the package is installed; if it is, don't install.
	# Otherwise, install (and maybe upgrade, due to dependency side effects).
	# Also, optimize so that we try to install or not install all the
	# packages at once if none are installed.
	are_packages_installed $@
	if [ $? -eq 1 ]; then
	    return 0
	fi

	retval=0
	while [ ! -z "$1" ] ; do
	    are_packages_installed $1
	    if [ $? -eq 0 ]; then
		$APTGETINSTALL $1
		retval=`expr $retval \| $?`
	    else
		retval=0
	    fi
	    shift
	done
	return $retval
    fi
}

do_dist_upgrade() {
    update_package_cache
    $SUDO apt-get $extraopts $DPKGOPTS $APTGETINSTALLOPTS dist-upgrade
}

do_security_upgrade() {
    [ -f /etc/apt/sources.list.security-only ] \
	&& $SUDO rm -f /etc/apt/sources.list.security-only
    grep security /etc/apt/sources.list | grep deb\ http \
	| $SUDO tee /etc/apt/sources.list.security-only
    for f in `ls -1 /etc/apt/sources.list.d/{emulab|powder}*.list` ; do
	[ -f $f ] && cat $f | $SUDO tee -a /etc/apt/sources.list.security-only
    done
    extraopts="-o Dir::Etc::SourceList=/etc/apt/sources.list.security-only"
    apt-get $extraopts $DPKGOPTS $APTGETINSTALLOPTS update \
	&& apt-get $extraopts $DPKGOPTS $APTGETINSTALLOPTS dist-upgrade
}
