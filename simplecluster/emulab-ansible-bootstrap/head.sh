#!/bin/sh

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/lib/lib.sh"
. "$DIRNAME/lib/lib-ansible.sh"

#
# Install Ansible.
#
if [ -n "$USE_SYSTEM_ANSIBLE" -a "$USE_SYSTEM_ANSIBLE" = "1" ]; then
    install_system_ansible
    if [ ! $? -eq 0 ]; then
	echo "ERROR: failed to install system ansible; aborting!"
	exit 1
    fi
else
    install_virtualenv && make_ansible_virtualenv
    if [ ! $? -eq 0 ]; then
	echo "ERROR: failed to install ansible virtualenv; aborting!"
	exit 1
    fi
fi

ANSIBLEPLAYBOOK=`get_ansible`
if [ -z "$ANSIBLEPLAYBOOK" ]; then
    echo "ERROR: ansible-playbook not available; aborting!"
    exit 1
fi
ANSIBLEGALAXY=`get_ansible_galaxy`
if [ -z "$ANSIBLEGALAXY" ]; then
    echo "ERROR: ansible-galaxy not available; aborting!"
    exit 1
fi

#
# Install the emulab.common collection according to the url/path in
# EMULAB_COLLECTION.  By default this is remote, but could point to a local
# submodule if useful.
#
$ANSIBLEGALAXY collection install "$EMULAB_COLLECTION" \
    || (echo "ERROR: failed to install emulab.common collection from $EMULAB_COLLECTION; aborting"; exit 1)
# XXX: really should use `ansible-galaxy collection list` to extract the
# installed path to find this file, but this is just fine for now.
pip install -r ~/.ansible/collections/ansible_collections/emulab/common/requirements.txt

#
# Initialize localhost with emulab-common role.  This bootstraps us to the
# point where we can grab the experiment manifest, generate ansible
# inventories, and assemble ansible-galaxy and ansible-playbook sequences.
#
# We are willing to make a default inventory, into which all nodes and
# groupings go.  Users can send nodes/groups to custom inventories.
#
cat <<EOF >$OURDIR/inventory-localhost.ini
[all]
localhost ansible_connection=local
EOF

$ANSIBLEPLAYBOOK -v -b -i $OURDIR/inventory-localhost.ini \
    -l localhost -e do_emulab_generate_automation=true \
    emulab.common.common \
    || exit 1

chmod 755 /local/setup/ansible/run-automation.sh

if [ -z "$EMULAB_ANSIBLE_NOAUTO" -o ! "$EMULAB_ANSIBLE_NOAUTO" = "1" ]; then
    exec /local/setup/ansible/run-automation.sh
else
    exit 0
fi
