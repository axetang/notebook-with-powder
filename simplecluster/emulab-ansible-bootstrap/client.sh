#!/bin/sh

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/lib/lib.sh"

KEYNAME=id_rsa_perexpt

mkdir -p ~/.ssh
chmod 700 ~/.ssh
rm -f ~/.ssh/${KEYNAME} ~/.ssh/${KEYNAME}.pub
geni-get key > ~/.ssh/$KEYNAME
chmod 600 ~/.ssh/${KEYNAME}
ssh-keygen -f ~/.ssh/${KEYNAME} -y > ~/.ssh/${KEYNAME}.pub
chmod 600 ~/.ssh/${KEYNAME}.pub
cat ~/.ssh/${KEYNAME}.pub >> ~/.ssh/authorized_keys2
chmod 600 ~/.ssh/authorized_keys2

exit 0
