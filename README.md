# Notebook with POWDER

This repository contains example notebooks and libraries for the experiment prototype.
The example notebooks can be found in `/portalxlivebook`.

Currently only notebook `iPerf3 Demo` is functioning.

## Getting started

- [ ] Install [Elixir](https://elixir-lang.org/install.html)
- [ ] Install [Livebook](https://livebook.dev/#install)

Once both are installed, use the following command to run Livebook on your computer.
```bash
livebook server --name mac@127.0.0.1
```

## Use iPerf3 Demo Notebook

Once the Livebook server is running, open the `iPerf3 Demo` notebook in Livebook.

To successfully allocate experiment resources using the notebook, you will need two decrypted certificates
signed by CloudLab. Once you have those two files, change the path to those credentials as indicated by the notebook.

## Grammar of the Prototype Workflow

The workflow of an experiment is defined in YAML. Below is an example workflow that defines an iPerf3 experiment.
```YAML
is: client-server
requires:
  client: c
  server: s
  server_teardown: st
cells:
    c:
        targets:
            - executer@node1.portalapitesting.powdersandbox.emulab.net
        commands:
            - iperf3 -c 192.168.1.2 -t 10
        output: true
    
    s:
        targets:
            - executer@node2.portalapitesting.powdersandbox.emulab.net
        commands:
            - iperf3 -s
        synchronous: false
    
    st:
        targets:
            - executer@node2.portalapitesting.powdersandbox.emulab.net
        commands:
            - pkill iperf3
```

There are a few mandatory key-value pairs: `is`, `requires`, and `cells`.

There are two possible values for the `is` keyword: `client-server` or `simple`.

`client-server` uses a predefined pattern that allows users to execute client-server structured experiments.
A user needs to specify different parts of the client and the server using `requires`.
Six possible parts can be specified: `server_setup`, `client_setup`, `server`, `client`, `server_teardown`, and `client_teardown`. At the time of running the experiment, these six parts will be executed in the following order.

```
server_setup -> server -> client_setup -> client -> client_teardown -> server_teardown
```

`simple` pattern is used for linearly-running experiments. A user only needs to specify `start`, which is the entry point of the whole experiment using `requires`.

`cells` defines a collection of atomic experiment procedures that are called 'cell'.

For each 'cell', a user can specify its `targets`, which is a list of hosts that this cell applies to.
A user can also specify a list of commands using `commands` keyword.

There are a few key-value pairs that are used to define different characters of a cell.

`output` can be `true` or `false`, which indicates if the outputs of this cell will be collected.

`synchronous` can be `true` or `false`, which indicates if the experiment manager should hang till all the results from commands are returned.

## TODO Add instructions for variables and conditional cells

