defmodule Experimentwizard do
  @moduledoc """
  Documentation for `Experimentwizard`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Experimentwizard.hello()
      :world

  """
  def hello do
    :world
  end
end
