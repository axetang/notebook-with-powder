defmodule PortalapiTest do
  import Portalapi
  use ExUnit.Case

  @test_user %{
    certfile: "/Users/atang/Documents/notebook-with-powder/portalapi/cloudlab-decrypted.pem",
    cacertfile: "/Users/atang/Documents/notebook-with-powder/portalapi/emulab-ca.pem"
  }
  @project_name "PowderSandbox"
  @experiment_name "PortalAPITesting"
  @profile_name "PowderSandbox,BandwidthSingle"

  # test "start_experiment" do
  #   Portalapi.start_experiment(@test_user, @profile_name, @project_name, @experiment_name) |> IO.inspect()
  # end

  # test "terminate_experiment" do
  #   Portalapi.terminate_experiment(@test_user, @project_name, @experiment_name) |> IO.inspect()
  # end

  # test "get_user_membership" do
  #   Portalapi.get_user_membership(@test_user) |> IO.inspect()
  # end

  # test "get_user_nodecount" do
  #   Portalapi.get_user_nodecount(@test_user) |> IO.inspect()
  # end

  # test "get_user_experiments" do
  #   Portalapi.get_user_experiments(@test_user) |> IO.inspect()
  # end

  # test "get_manifest" do
  #   Portalapi.get_manifest(@test_user, @project_name, @experiment_name) |> IO.inspect()
  # end

  # test "get_experiment_status" do
  #   Portalapi.get_experiment_status(@test_user, @project_name, @experiment_name) |> IO.inspect()
  # end

  # test "check_experiment_ready" do
  #   Portalapi.is_experiment_ready(@test_user, @project_name, @experiment_name) |> IO.inspect()
  # end
end
